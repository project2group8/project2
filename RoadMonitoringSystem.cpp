#include "RoadMonitoringSystem.h"

CascadeClassifier trafficSign_cascade;

// enable one vibration motor, you must be super user to run this function
void vibrationLow()
{
	cout << "lane is changed. (dashed-lane, low vibration)\n";
#ifdef LINUX	
	wiringPiSetup () ;
	pinMode (1, OUTPUT) ;

	digitalWrite (1, HIGH) ;	// On
	delay (500) ;		// mS
	digitalWrite (1, LOW) ;	// Off
#endif	
	cout << "end vibration\n";
}

//enable two vibration motor, you must be super user to run this function
void vibrationHigh()
{
	cout << "lane is changed. (Incorrect motion, high vibration)\n";
#ifdef LINUX	
	wiringPiSetup();
	pinMode(1, OUTPUT);
	pinMode(4, OUTPUT);

	digitalWrite(1, HIGH);	// On
	digitalWrite(4, HIGH);	// On
	delay(500);		// mS
	digitalWrite(1, LOW);	// Off
	digitalWrite(4, LOW);	// On
#endif
	cout << "end vibration\n";
}


#ifdef DEBUG
/* display point that is clicked */
void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
	if (event == EVENT_LBUTTONDOWN)
		cout << "coordinate (" << x << ", " << y << ")" << endl;
}
#endif

int RoadMonitoringSystem::start()
{
	Mat edges;
	int outLoopIndex = 0, speedUp = 0, numberOfSign = 0;

	bool enableLineDetection = true, enableWeatherDetection = true,
		enableSpeedDetection = true, enableSignDetection = true;

	help(); // show hotkeys

	if (!trafficSign_cascade.load(XML_PATH))
	{
		cerr << "Error: " << XML_PATH << " is not loaded!\n";
		return -3;
	}

#ifndef CAMERA
	VideoCapture cap(videoPath); // open the default camera
#endif
#ifdef CAMERA
	VideoCapture cap(0); // open the default camera
#endif

	if (!cap.isOpened()) // check if we succeeded
	{
		#ifdef CAMERA
			cerr << "Error: camera module is not found!\n";
		#endif
		#ifndef CAMERA
			cerr << "Error: " << VIDEO_PATH << "is not found!\n";
		#endif
		return -1;
	}
	
	// forward video to given minute and second
#ifndef CAMERA
	int forwardTime = FORWARD_MINUTE * 60 * 1000 + FORWARD_SECOND * 1000;
	cap.set(CV_CAP_PROP_POS_MSEC, forwardTime);
#endif

#ifdef SPEED_DETECTION
	/*For Direction and Speed*/
	int speed = 0, dirctionSum = 80, countZero = 0;
	vector <int> directionVec, speedVec;
	UMat gray, prevgray;
	SpeedAndDirection calcSpeedAndDirection;
	calcSpeedAndDirection.speedStatus = 0;
	for (int i = 0; i < DIRECTION_VEC_SIZE; ++i)
		directionVec.push_back(0);
	for (int i = 0; i < SPEED_VEC_SIZE; ++i)
		speedVec.push_back(0);
#endif

	// for weather detection
	int dayNightSection[DAYNIGHTARRAY_SIZE];
	for (int i = 0; i < DAYNIGHTARRAY_SIZE; ++i){
		if (i < DAYNIGHTARRAY_SIZE / 3)
			dayNightSection[i] = 0; // 0 is sunlight
		if (i >= DAYNIGHTARRAY_SIZE / 3 && i < (2 * DAYNIGHTARRAY_SIZE) / 3)
			dayNightSection[i] = 1; // 1 is daylight
		if (i >= (2 * DAYNIGHTARRAY_SIZE) / 3)
			dayNightSection[i] = 2; // 2 is nightlight
	}

	//		*-*-*-*-*-*-*   M A I N      L O O P   *-*-*-*-*-*-*		//
	for (int frameIndex = 0;; ++outLoopIndex)
	{

		Mat frame;

		string topLeftText = "";

#ifdef DEBUG
		Mat dst;
		char toStr[4];
		sprintf(toStr, "%d", speedUp + 1);
		topLeftText.append("x").append(toStr);
		// set mouse listener to display clicked point on console
		setMouseCallback(FRAME_NAME, CallBackFunc, NULL);
#endif

		// to speed up for local video, press button '+'
		for (int i = 0; i < FORWARD_FRAME_PER_LOOP + speedUp; ++i, ++frameIndex)
		{
			cap >> frame; // get a new frame from camera
			if (!frame.isContinuous() || frame.empty()) // stop video if it reaches end
				return 1;
		}

#ifdef SPEED_DETECTION
		resize(frame, gray, Size(300, 300));
		cvtColor(gray, gray, COLOR_BGR2GRAY);
#endif

		cv::resize(frame, frame, cv::Size(720, 480));

		// sign detection
		if (enableSignDetection)
		{
			#ifdef SIGN_DETECTION_WITH_HAAR_CASCADE
			trafficSignDetectionWithHaarCascade(frame);
			#endif
			
			#ifdef SIGN_DETECTION_WITH_COUNTOURS
			trafficSignDetectionWithContours(frame,frame); 
			#endif
			topLeftText.append(" signDetection: enable ");
		}
		else
			topLeftText.append(" signDetection: disable ");

#ifdef DEBUG
		// calculate current time and show it on top left
		int currentTime = forwardTime / 1000 + frameIndex / VIDEO_FPS;
		int currentSecond = currentTime % 60;
		int currentMinute = currentTime / 60;
		char currentMinuteStr[10];
		char currentSecondStr[3];
		sprintf(currentMinuteStr, "%02d", currentMinute);
		sprintf(currentSecondStr, "%02d", currentSecond);
		string currentTimeStr = currentMinuteStr; currentTimeStr.append(":");
		currentTimeStr.append(currentSecondStr);
		topLeftText.append(" time: ").append(currentTimeStr);
#endif

		string weatherSituation;
		if (enableWeatherDetection)
		{
			topLeftText.append(" weather: ");
			RoadMonitoringSystem::weatherDetection(frame, dayNightSection, outLoopIndex, weatherSituation);
			topLeftText.append(weatherSituation);
		}
		else
			topLeftText.append(" weather: disable ");

		if (enableLineDetection)
		{
			topLeftText += " line: shown ";
			/*std::thread lineDetectionThread(&RoadMonitoringSystem::lineDetectionThreadFunc, std::ref(frame));
			lineDetectionThread.join();*/
#ifdef DEBUG
			Canny(frame, dst, 20, 100, 3);
			imshow("canny", dst);
#endif
			RoadMonitoringSystem::lineDetectionThreadFunc(frame);
		}
		else
			topLeftText += " line: disable ";

#ifdef SPEED_DETECTION
		if (enableSpeedDetection)
		{
			string prevText = topLeftText;
			topLeftText += " speed: calculating...";
			if (!prevgray.empty()){
				calcualteSpeedAndDirection(&calcSpeedAndDirection, prevgray, gray, &speed, &countZero, &directionVec, &dirctionSum);
				//	std::thread t(&RoadMonitoringSystem::threadForDirectionAndSpeed,this,&calcSpeedAndDirection, prevgray, gray, &speed, &countZero, &directionVec, &dirctionSum );
				//t.join();	
				int speedAvarage = 0;
				for (int i = 0; i < SPEED_VEC_SIZE; ++i)
					speedAvarage += speedVec.at(i);
				if (calcSpeedAndDirection.speedStatus < 120){
					speedAvarage += calcSpeedAndDirection.speedStatus;
					speedAvarage /= SPEED_VEC_SIZE + 1;
				}
				else
					speedAvarage /= SPEED_VEC_SIZE;
				//cout << "frame speed:" << calcSpeedAndDirection.speedStatus << endl;

				speedVec.erase(speedVec.begin());
				speedVec.push_back(speedAvarage);
				topLeftText = prevText;
				topLeftText += " speed: " + std::to_string(speedAvarage);

				cv::arrowedLine(frame, Point(80, 80), Point(calcSpeedAndDirection.directionStatus, 30), Scalar(255, 0, 0), 10, 8, 0, 0.5);
				calcSpeedAndDirection.directionStatus = 0;
			}
			//std::swap(prevgray, gray);
			gray.copyTo(prevgray);
		}
		else
			topLeftText += " speed: disable ";
#endif

		putText(frame, topLeftText, Point(10, frame.rows - 10), FONT_HERSHEY_COMPLEX, 0.5, Scalar(255, 255, 255), 1, LINE_AA);

		imshow(FRAME_NAME, frame);

		int key = waitKey(DELAY_FRAME);
		key %= 256; // for linux os, it must be got mod
		if (key == 27) // press ESC to exit
		{
			cout << "Terminating program..\n";
			return 1;
		}
#ifdef DEBUG
		else if (key == 43) // press + to speed up video
			++speedUp;
		else if (key == 45 && speedUp > 0) //press - to slow down video
			--speedUp;
#endif
		else if (key == 'P' || key == 'p') // press P to pause
		while (waitKey(0) != key);
		else if (key == 'l' || key == 'L') // press L to enable line detection
			enableLineDetection = enableLineDetection ? false : true;
		else if (key == 'w' || key == 'W') // press W to enable weather detection
			enableWeatherDetection = enableWeatherDetection ? false : true;
		else if (key == 'S' || key == 's') // press S to show speed and direction
			enableSpeedDetection = enableSpeedDetection ? false : true;
		else if (key == 'T' || key == 't') // press T to show sign detection
			enableSignDetection = enableSignDetection ? false : true;
		else if (key == 'C' || key == 'c') // press C to show contributors
			cout << "Contributors:\n Sevgi Borazan\n Huseyin Cure\n Omer Coskuncelebi\n Busra Erkan\n Halil Oymaci\n Begum Sozen\n Samet Yuksel\n";
	}
	cout << "End of the video\n";
	return 0;
}

/* Display welcome message and hotkeys */
void RoadMonitoringSystem::help()
{
	cout << "\nThis is a Road Monitoring System,\n"
		"Line, Weather, Sign, Speed and Direction Detection\n"
		"Using OpenCV version " << CV_VERSION << endl;
	cout << "\nHot keys: \n"
		"\tL - \"line detection\" on/off\n"
		"\tW - \"weather detection\" on/off\n"
		"\tS - speed and direction detection on/off\n"
		"\tT - sign detection on/off\n"
		"\tC - Show contributors\n"
		"\tP - pause video\n"
		"\tESC - quit the program\n";
#ifdef DEBUG
	cout << "DEBUG mode is enable.\n";
#endif
}

/* calculate average RGB values of the given line */
int RoadMonitoringSystem::getAverageLineColor(cv::Mat frame, Point start, Point end)
{
	LineIterator it(frame, start, end, 8);
	vector<Vec3b> buf(it.count);
	int averageColor, totalAverage = 0, i = 0;
	for (; i < it.count; i++, ++it)
	{
		buf[i] = (const Vec3b)*it;
		averageColor = buf[i][0];
		averageColor += buf[i][1];
		averageColor += buf[i][2];
		averageColor /= 3;
		totalAverage += averageColor;
	}
	return totalAverage / i;
}

int RoadMonitoringSystem::trafficSignDetectionWithContours(Mat src, Mat &out)
{
	Mat source = src.clone(), cany;
	Canny(src, cany, 50, 200, 3);

	// Find contours
	std::vector<std::vector<cv::Point> > contours;
	cv::findContours(cany, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	std::vector<cv::Point> approx;
	out = source.clone();

	int numberOfSign = 0;

	for (int i = 0; i < contours.size(); i++)
	{
		// Approximate contour with accuracy proportional
		// to the contour perimeter
		cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);

		// Skip small or non-convex objects 
		if (std::fabs(cv::contourArea(contours[i])) < 100 || !cv::isContourConvex(approx))
			continue;

		if (approx.size() == 3)
		{
			setLabel(out, "LEVHA", contours[i]);    // Triangles
			++numberOfSign;
		}
		else if (approx.size() >= 4 && approx.size() <= 6)
		{
			// Number of vertices of polygonal curve
			size_t vtc = approx.size();

			// Get the cosines of all corners
			std::vector<double> cos;
			for (int j = 2; j < vtc + 1; j++)
				cos.push_back(angle(approx[j%vtc], approx[j - 2], approx[j - 1]));

			// Sort ascending the cosine values
			std::sort(cos.begin(), cos.end());

			// Get the lowest and the highest cosine
			double mincos = cos.front();
			double maxcos = cos.back();

			// Use the degrees obtained above and the number of vertices
			// to determine the shape of the contour
			if (vtc == 4 && mincos >= -0.1 && maxcos <= 0.3)
			{
				setLabel(out, "LEVHA", contours[i]);
				++numberOfSign;
			}
		}
		else
		{
			// Detect and label circles
			double area = cv::contourArea(contours[i]);
			cv::Rect r = cv::boundingRect(contours[i]);
			int radius = r.width / 2;

			if (std::abs(1 - ((double)r.width / r.height)) <= 0.2 && std::abs(1 - (area / (CV_PI * std::pow(radius, 2)))) <= 0.2)
			{
				setLabel(out, "LEVHA", contours[i]);
				++numberOfSign;
			}
		}
	}
	return numberOfSign;
}

double RoadMonitoringSystem::angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;
	return (dx1*dx2 + dy1*dy2) / sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

/**
* Helper function to display text in the center of a contour
*/
void RoadMonitoringSystem::setLabel(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour)
{
	int fontface = cv::FONT_HERSHEY_SIMPLEX;
	double scale = 0.4;
	int thickness = 1;
	int baseline = 0;

	cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
	cv::Rect r = cv::boundingRect(contour);

	cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
	cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255, 255, 255), CV_FILLED);
	cv::putText(im, label, pt, fontface, scale, CV_RGB(0, 0, 0), thickness, 8);
}

// calculate average color of the given lane (road)
int RoadMonitoringSystem::getAverageLaneColor(cv::Mat frm, Vec4i start, Vec4i end)
{
	//int averageLineColor = getAverageLineColor(frame, Point(start[0], start[1]), Point(start[2], start[3]));

	// Define a polygon
	Point pts[1][4];
	pts[0][2] = Point(start[0], start[1]);
	pts[0][3] = Point(start[2], start[3]);
	pts[0][1] = Point(end[0], end[1]);
	pts[0][0] = Point(end[2], end[3]);

	const Point* points[1] = { pts[0] };
	int npoints = 4;

	// Create the mask with the polygon
	Mat1b mask(frm.rows, frm.cols, uchar(0));
	fillPoly(mask, points, &npoints, 1, Scalar(255));

	Scalar average = mean(frm, mask);
	int avc = (int)((average[0] + average[1] + average[2]) / 3);
	if (avc > LINE_COLOR)
		fillPoly(frm, points, &npoints, 1, Scalar(255, 0, 255));
	return avc;
}

int RoadMonitoringSystem::findLineLength(Vec4i line)
{
	return (int)sqrt(pow((line[0] - line[2]), 2) + pow((line[1] - line[3]), 2));
}

double RoadMonitoringSystem::findLineAngle(Vec4i line)
{
	if (line[0] != line[2])
	{
		double slope = (line[1] - line[3]) / (double)(line[0] - line[2]);
		return atan(slope) * 180 / CV_PI;
	}
	else
		return 90;
}

void RoadMonitoringSystem::lineDetectionThreadFunc(Mat &frame)
{
	Mat cpyFrame, dst, cdst;
#ifdef CROP_HEIGHT_RATE
	cv::Rect myROI(0, (int)(frame.rows*CROP_HEIGHT_RATE), frame.cols, (int)(frame.rows*(1 - CROP_HEIGHT_RATE)));
	cpyFrame = frame(myROI);
	Canny(cpyFrame, dst, 20, 100, 3);
#endif
#ifndef CROP_HEIGHT_RATE
	Canny(frame, dst, 50, 150, 3);
#endif
	/*imshow("cany90-200", dst);
	Canny(frame, dst, 50, 200, 3);
	imshow("cany50-300", dst);*/
	//cvtColor(dst, cdst, COLOR_GRAY2BGR);

	bool thereIsOneLine = false;
	vector<Vec4i> lines2;

	vector<Vec4i> lines;
	HoughLinesP(dst, lines, 1, CV_PI / 180, 80, LINE_MIN_LENGTH, 10);

#ifdef DEBUG
	//middle vertical line
	//line(frame, Point(frame.cols / 2, 0), Point(frame.cols / 2, frame.rows), CV_RGB(255, 255, 0), 2, cv::LineTypes::LINE_AA);

	// vibration thread control lines
	line(frame, Point(frame.cols * 40 / 100, frame.rows*VIBRATION_HORIZONTOL_START_RATE_FROM_TOP), Point(frame.cols * 60 / 100, frame.rows*VIBRATION_HORIZONTOL_START_RATE_FROM_TOP), CV_RGB(0, 255, 0), 2, cv::LineTypes::LINE_AA);
	line(frame, Point(frame.cols * 40 / 100, frame.rows*VIBRATION_HORIZONTOL_START_RATE_FROM_TOP), Point(frame.cols * 40 / 100, frame.rows), CV_RGB(0, 255, 0), 2, cv::LineTypes::LINE_AA);
	line(frame, Point(frame.cols * 60 / 100, frame.rows*VIBRATION_HORIZONTOL_START_RATE_FROM_TOP), Point(frame.cols * 60 / 100, frame.rows), CV_RGB(0, 255, 0), 2, cv::LineTypes::LINE_AA);

	// the horizontol line of start from top to find lane
	line(frame, Point(0, frame.rows*CROP_HEIGHT_RATE), Point(frame.cols, frame.rows*CROP_HEIGHT_RATE), CV_RGB(255, 255, 0), 2, cv::LineTypes::LINE_AA);
#endif

	int leftSide = 0, rightSide = 0;

	for (size_t i = 0; i < lines.size(); i++)
	{
		Vec4i l = lines[i];
		l[1] += (int)(frame.rows *CROP_HEIGHT_RATE);
		l[3] += (int)(frame.rows *CROP_HEIGHT_RATE);

		if (l[0]>frame.cols / 2 && l[2] > frame.cols / 2)
		{
			if (rightSide == 2)
				continue;
			else
				++rightSide;
		}

		else if (l[0] < frame.cols / 2 && l[2] < frame.cols / 2)
		{
			if (leftSide == 2)
				continue;
			else
				++leftSide;
		}

		// find line slope
		double angle = RoadMonitoringSystem::findLineAngle(l);
		// if line is approximately horizontal, reject it
		if ((abs(angle) > LINE_MIN_SLOPE) && (abs(angle) < LINE_MAX_SLOPE))
		{
			int averageLineColor = RoadMonitoringSystem::getAverageLineColor(frame, Point(l[0], l[1]), Point(l[2], l[3]));

			if (averageLineColor > LINE_COLOR)
			{
#ifdef DEBUG
#ifdef LINE_INFORMATION
				double length = RoadMonitoringSystem::findLineLength(l);
				cout << setw(2) << i << ": s(" << setw(3) << l[0] << "," << setw(3) << l[1] << ") e(" << setw(3) << l[2] << "," << setw(3)
					<< l[3] << ")" << " rgb: " << " angle: " << setw(8) << angle << " length: " << (int)length << endl;
#endif
#endif
				line(frame, Point(l[0], l[1]), Point(l[2], l[3]), CV_RGB(255, 0, 0), 3, cv::LineTypes::LINE_AA);
#ifdef DEBUG
				circle(frame, Point(l[0], l[1]), 1, Scalar(255, 0, 0), 5, CV_AA);
				circle(frame, Point(l[2], l[3]), 1, Scalar(255, 255, 255), 5, CV_AA);
#endif
				// line breatch, execute vibration thread
				if (((l[0] > frame.cols * 2 / 5 && l[0] <  frame.cols * 3 / 5) && (l[2] >  frame.cols * 2 / 5 && l[2] < frame.cols * 3 / 5)))
				{
					if (l[1] > frame.rows * VIBRATION_HORIZONTOL_START_RATE_FROM_TOP || l[3] > frame.rows * VIBRATION_HORIZONTOL_START_RATE_FROM_TOP){
						double length = RoadMonitoringSystem::findLineLength(l);
						if (length > 150)
						{
							std::thread vibrationThread(&vibrationHigh);     // spawn new thread that calls vibration()
#ifdef DEBUG
							vibrationThread.join();
#endif
#ifndef DEBUG
							vibrationThread.detach();
#endif
						}
						else if (length > 50)
						{
							std::thread vibrationThread(&vibrationLow);     // spawn new thread that calls vibration()
#ifdef DEBUG
							vibrationThread.join();
#endif
#ifndef DEBUG
							vibrationThread.detach();
#endif
						}
					}
				}
			}
		}
	}
#ifdef DEBUG
#ifdef LINE_INFORMATION
	cout << "------------\n";
#endif
#endif
}

/*******************CALCULATE  DIRECTION***************************/

double RoadMonitoringSystem::calculateAngele(Point2f f, Point2f s){

	return atan(((s.y - f.y) / (s.x - f.x))) * 180.0 / CV_PI;
}
SpeedAndDirection RoadMonitoringSystem::drawOptFlowMap(const Mat& flow, Mat& cflowmap, int step, const Scalar& color) {
	double angle = 0.0;
	int count = 0, count2 = 0, count3 = 0, count4 = 0, total = 0, left = 0, right = 0;
	double length = 0.0, max = 0.0;
	SpeedAndDirection calcSpeedAndDirection;
	calcSpeedAndDirection.directionStatus = -1;
	calcSpeedAndDirection.speedStatus = 0;

	for (int y = 30; y < cflowmap.rows; y += step){  //y 30
		for (int x = 0; x < cflowmap.cols; x += step){
			const Point2f& fxy = flow.at< Point2f>(y, x);
			angle = calculateAngele(Point(x, y), Point(cvRound(x + fxy.x), cvRound(y + fxy.y)));
			//	if (y > 200){
			if ((angle > 0 || angle < -2)){
				++total;
				if (angle <= -2 && angle >= -45){
					//if (y > 200)
					count++;
					/*else
					++count3;*/
					cv::arrowedLine(cflowmap, Point(x, y), Point(cvRound(x + fxy.x), cvRound(y + fxy.y)), Scalar(255, 0, 0), 5, 4, 0, 0.5);

				}
				else if (angle < -45 && angle >= -90){
					count2++;
					cv::arrowedLine(cflowmap, Point(x, y), Point(cvRound(x + fxy.x), cvRound(y + fxy.y)), Scalar(0, 255, 0), 5, 4, 0, 0.5);
				}
				else if (angle >= 0 && angle <= 50){
					cv::arrowedLine(cflowmap, Point(x, y), Point(cvRound(x + fxy.x), cvRound(y + fxy.y)), Scalar(0, 0, 255), 5, 4, 0, 0.5);
					//	if (y > 200)
					count3++;
					/*else
					++count;*/
				}
				else if (angle > 50 && angle <= 90){
					cv::arrowedLine(cflowmap, Point(x, y), Point(cvRound(x + fxy.x), cvRound(y + fxy.y)), Scalar(10, 25, 50), 5, 4, 0, 0.5);
					count4++;
				}
			}
			//}
			if (angle != 0/* && x >= 70*/ && abs(cvRound(y + fxy.y) - y) > 2){

				length = sqrt((abs(cvRound(x + fxy.x) - x)* (abs(cvRound(x + fxy.x) - x))) + (abs(cvRound(y + fxy.y) - y)*abs(cvRound(y + fxy.y) - y)));
				if (max < length){
					max = length;
					//max /= 2;
					//	line(cflowmap, Point(x, y), Point(cvRound(x + fxy.x), cvRound(y + fxy.y)), Scalar(153, 0,153));
				}
			}

			//	line(cflowmap, Point(x, y), Point(cvRound(x + fxy.x), cvRound(y + fxy.y)), Scalar(0, 255, 0));
			cv::circle(cflowmap, Point(cvRound(x + fxy.x), cvRound(y + fxy.y)), 1, Scalar(255, 0, 0), -1);
		}
	}

	if (count3 != 0 && count != 0){
		left = (count3 + count4) / (count + count2);//(count3 + count4) / (count + count2)
		right = (count2 + count) / (count3 + count4);//(count2 + count) / (count3 + count4)
		/*left = count3 / (count3 + count);
		right = count / (count3 + count);*/
		///cout << "KIRMIZZZIII         " << count3/count << endl;
		///	cout << "kirmiziL  " << left << " maviR " << right << endl;

	}
	if (total != 0){
		//cout << "ANGLEEEE     " << count << "    " << count2 << "    " << count3 << "    " << count4 << endl; */
		if ((left > 2 && right < 1)){
			calcSpeedAndDirection.directionStatus = 10; //left //1 //-10
		}
		else if (right > 2 && left < 1){
			calcSpeedAndDirection.directionStatus = -10; //right //2 //+10

		}
		else if (right <= 1 && left <= 1)
			calcSpeedAndDirection.directionStatus = 0;//straight
	}

	calcSpeedAndDirection.speedStatus = (int)((max)* 100 * 0.04);
	//cout << "Hizzz : " << calcSpeedAndDirection.speedStatus << endl;
	return calcSpeedAndDirection;
}
void RoadMonitoringSystem::calcualteSpeedAndDirection(SpeedAndDirection *calcSpeedAndDirection, UMat prevFrame, UMat nextFrame, int* speed, int* countZero, vector<int>*directionVec, int* dirctionSum){
	Mat flow, cflow;
	Mat uflow;
	int countDirection = 0;
	calcOpticalFlowFarneback(prevFrame, nextFrame, uflow, 0.5, 3, 15, 3, 5, 1.2, 0);
	cvtColor(prevFrame, cflow, COLOR_GRAY2BGR);
	uflow.copyTo(flow);
	*calcSpeedAndDirection = drawOptFlowMap(flow, cflow, 10, CV_RGB(0, 255, 0));

	if (calcSpeedAndDirection->speedStatus < 3)
		++(*countZero);
	/*if ((*countZero) > 2)
		*speed = 0;*/

	else if ((*speed) > 1){
		if (abs(calcSpeedAndDirection->speedStatus - (*speed)) < 30){

			(*speed) += calcSpeedAndDirection->speedStatus;
			(*speed) /= 2;
			--(*countZero);
		}
	}
	else
		(*speed) = calcSpeedAndDirection->speedStatus;


	if (calcSpeedAndDirection->directionStatus != -1 && (*speed) != 0){
		countDirection = 0;
		for (int i = 0; i < DIRECTION_VEC_SIZE; ++i)
		if (calcSpeedAndDirection->directionStatus == (*directionVec).at(i))
			++countDirection;
		if (countDirection == DIRECTION_VEC_SIZE)
			(*dirctionSum) = 80 + calcSpeedAndDirection->directionStatus;
		(*directionVec).erase((*directionVec).begin());
		(*directionVec).push_back(calcSpeedAndDirection->directionStatus);

	}
	cv::arrowedLine(cflow, Point(80, 80), Point(*dirctionSum, 30), Scalar(255, 0, 0), 10, 8, 0, 0.5);
#ifdef DEBUG
	imshow("OPTIC_FLOW", cflow);
#endif
	calcSpeedAndDirection->speedStatus = (*speed);
	calcSpeedAndDirection->directionStatus = (*dirctionSum);

}
void RoadMonitoringSystem::threadForDirectionAndSpeed(SpeedAndDirection *calcSpeedAndDirection, UMat prevFrame, UMat nextFrame, int* speed, int* countZero, vector<int> *directionVec, int* dirctionSum){
	calcualteSpeedAndDirection(calcSpeedAndDirection, prevFrame, nextFrame, speed, countZero, directionVec, dirctionSum);
}

/*void lineDetection(Mat& frame)
{
lineDetectionThreadFunc(frame);
}*/

void RoadMonitoringSystem::weatherDetection(Mat& frame, int dayNightSection[], int index, string& output)
{
	int blue = 0, green = 0, red = 0;
	for (int i = 0; i < frame.rows; i = i + 4)
	for (int j = 0; j < frame.cols; j = j + 4)
	{
		Vec3b color = frame.at<Vec3b>(Point(j, i));
		blue += color.val[0];
		green += color.val[1];
		red += color.val[2];
	}

	blue = (blue * 16) / (frame.rows*frame.cols);
	green = (green * 16) / (frame.rows*frame.cols);
	red = (red * 16) / (frame.rows*frame.cols);

	int average = (blue + green + red) / 3;
	
	//cerr << average << endl;

	dayNightSection[index % DAYNIGHTARRAY_SIZE] = 0;

	if (average<NIGHTTIME_AVERAGE_RGB)
		dayNightSection[index % DAYNIGHTARRAY_SIZE] = 0;
	else if (average>NIGHTTIME_AVERAGE_RGB && average < DAYTIME_AVERAGE_RGB)
		dayNightSection[index % DAYNIGHTARRAY_SIZE] = 1;
	else
		dayNightSection[index % DAYNIGHTARRAY_SIZE] = 2;

	int sunLightNumber = 0;
	int dayLightNumber = 0;
	int nightLightNumber = 0;

	for (int i = 0; i < DAYNIGHTARRAY_SIZE; ++i){
		if (dayNightSection[i] == 0)
			++nightLightNumber;
		else if (dayNightSection[i] == 1)
			++dayLightNumber;
		else
			++sunLightNumber;
	}

	if (sunLightNumber > dayLightNumber && sunLightNumber > nightLightNumber)
		output = "sunLight";
	else if (dayLightNumber > sunLightNumber && dayLightNumber > nightLightNumber)
		output = "dayLight";
	else
		output = "nightLight";

	char averageStr[4];
	std::sprintf(averageStr, "%d", average);

#ifdef DEBUG
	output.append("(").append(averageStr).append(")");
#endif

}

void RoadMonitoringSystem::trafficSignDetectionWithHaarCascade(Mat frame) {
	vector<Rect> trafficSigns;
	Mat frame_gray;
	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);
	trafficSign_cascade.detectMultiScale(frame_gray, trafficSigns, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(30, 30));
	for (size_t i = 0; i < trafficSigns.size(); i++)
		rectangle(frame, Point(trafficSigns[i].x, trafficSigns[i].y), Point(trafficSigns[i].x + trafficSigns[i].width, trafficSigns[i].y + trafficSigns[i].height), Scalar(0, 255, 0), 2);
}
