#ifndef ROAD_MONITORING_SYSTEM
#define ROAD_MONITORING_SYSTEM

#include "opencv2/highgui/highgui.hpp" // videoCapture, waitKey, imshow
#include "opencv2/imgproc/imgproc.hpp" // houghlinesP, canny, putText, line
#include <opencv2/video.hpp> // calcOpticalFlowFarneback
#include "opencv2/objdetect/objdetect.hpp" //CascadeClassifier
#include <iostream> // cout, endl
#include <string> // string
#include <iomanip> // setw
#include <thread> // thread
#include <chrono> // milliseconds

#define LINUX

//wiringpi, gpio on-off
#ifdef LINUX
#include <wiringPi.h>
#endif

//#define DEBUG

#define CAMERA // if this is defined, get frame from default camera machine, if it is not defined, get video from VIDEO_PATH
#define FRAME_NAME "Road Monitoring System"
#define VIDEO_PATH "/home/orangepi/Desktop/final_video.avi" // you can change video start time above FORWARD macros
#define DELAY_FRAME 1 // delay each frame as milisecond

// this is used for sign detection
#define XML_PATH "/home/orangepi/Desktop/RoadMonitoringSystem/project2/signDetection.xml"
#define SIGN_DETECTION_WITH_HAAR_CASCADE
//#define SIGN_DETECTION_WITH_COUNTOURS

// you can use this macros if you use video from path, 19:30 is default because there is a lane changing at that moment
#define FORWARD_MINUTE 19 // go to xx minute in video
#define FORWARD_SECOND 30  // go to xx second in video
#define FORWARD_FRAME_PER_LOOP 1 // speed up video by given times, default: 1

// line detection defines
#define LINE_MIN_SLOPE 20 // show lines whose slope is greater than this
#define LINE_MAX_SLOPE 85// show lines whose slope is less than this
#define LINE_COLOR -1 // show line whose average color is greater than this
#define LINE_MIN_LENGTH 15
#define VIBRATION_HORIZONTOL_START_RATE_FROM_TOP 35/50
//#define LINE_INFORMATION // cout line start and end point and their color and length
#define CROP_HEIGHT_RATE 0.5 // crop height of the frame, discard top of the frame

// weather detection defines
#define DAYTIME_AVERAGE_RGB 160 // average RGB value of daytime color
#define NIGHTTIME_AVERAGE_RGB 100 // average RGB value of nighttime color
#define DAYNIGHTARRAY_SIZE 200 // number of frame which needs calculating weather detection

// speed and direction detection
#define SPEED_DETECTION
#define DIRECTION_VEC_SIZE 5
#define SPEED_VEC_SIZE 15

struct SpeedAndDirection{
	int speedStatus;
	int directionStatus;
};

using namespace cv;
using namespace std;

class RoadMonitoringSystem
{
public:
	RoadMonitoringSystem(std::string videoPath){ this->videoPath = videoPath; }
	int start();
	void help();
	int findSpeedAsDegree(Mat backgroundFrame, Vec4i line);
	void setLabel(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour);
	double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0);

	// line detection functions
	static void lineDetectionThreadFunc(Mat &frame);
	static int getAverageLaneColor(cv::Mat frm, Vec4i start, Vec4i end);
	static int findLineLength(Vec4i line);
	static double findLineAngle(Vec4i line);
	static int getAverageLineColor(cv::Mat frame, Point start, Point end);

	static void weatherDetection(Mat& frame, int dayNightSection[], int index, std::string& output);

	int trafficSignDetectionWithContours(Mat src, Mat &out);
	void trafficSignDetectionWithHaarCascade(Mat frame);

	// speed and direction detection functions
	SpeedAndDirection drawOptFlowMap(const Mat& flow, Mat& cflowmap, int step, const Scalar& color);
	double calculateAngele(Point2f f, Point2f s);
	void threadForDirectionAndSpeed(SpeedAndDirection *calcSpeedAndDirection, UMat prevFrame, UMat nextFrame, int* speed, int* countZero, vector<int> *directionVec, int* dirctionSum);
	void calcualteSpeedAndDirection(SpeedAndDirection *calcSpeedAndDirection, UMat prevFrame, UMat nextFrame, int* speed, int* countZero, vector<int> *directionVec, int* dirctionSum);

private:
	string videoPath;
	int direction;
};

#endif // !ROAD_MONITORING_SYSTEM
