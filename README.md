# README #

Road Monitoring System for CSE396 (Project-2).

Team Members

* Halil İbrahim Oymacı
* Hüseyin Berk Cüre
* Begüm Sözen
* Büşra Erkan
* Samet Yüksel
* Ömer Çoşkunçelebi
* Sevgi Borazan

### Features ###

* Lane Detection
* Sign Detection
* Speed Detection
* extra-feature: weather detection (sunlight,daylight,nightlight)
* extra-feature: road direction detection

### Project Website ###

http://gtu-project2-group8.magix.net/

### INSTALLATION and RUNNING ###

to compile, run this command:

make

then to execute program:

make run