# RoadMonitoringSystem makefile, after the make command, for running project, make run
all:
	g++ main.cpp RoadMonitoringSystem.cpp -c -std=c++11
	g++ main.o RoadMonitoringSystem.o -o exec -lopencv_highgui -lopencv_imgproc -lopencv_imgcodecs -lopencv_videoio -lopencv_video -lopencv_core -lopencv_objdetect -pthread -lwiringPi -lwiringPiDev -lpthread -lm
	
clean: ; rm -f main.o RoadMonitoringSystem.o exec 

run:
	sudo ./exec
